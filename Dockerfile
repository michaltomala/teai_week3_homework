FROM openjdk:8-jdk-alpine

RUN apk update && apk upgrade && \
    apk add maven

COPY ./ ./
WORKDIR ./

RUN mvn clean package

#RUN apk del maven

EXPOSE 8080
CMD cd /target && \
    java -jar /target/week3-0.0.1-SNAPSHOT.jar



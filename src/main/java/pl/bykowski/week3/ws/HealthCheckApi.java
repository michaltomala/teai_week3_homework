package pl.bykowski.week3.ws;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

public interface HealthCheckApi {

    @GetMapping
    public ResponseEntity<String> checkApp();
}

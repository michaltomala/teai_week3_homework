package pl.bykowski.week3.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/health-check", produces = MediaType.APPLICATION_JSON_VALUE)
public class HealthCheckController implements HealthCheckApi {

    @Value("${environment}")
    private String environment;

    private final BuildProperties buildProperties;

    @Autowired
    public HealthCheckController(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Override
    public ResponseEntity<String> checkApp() {
        return ResponseEntity.ok("environment: " + environment + ", version: " + buildProperties.getVersion());
    }

}
